//
//  ProjectViewController.swift
//  ProjectStream
//
//  Created by Dora Franjic on 05/10/2019.
//  Copyright © 2019 Dora Franjic. All rights reserved.
//

import UIKit

class ProjectViewController: UIViewController {
    
    @IBOutlet weak var playerView: UIView!
    
    @IBOutlet weak var thirdPlayerView: UIView!
    @IBOutlet weak var secondPlayerView: UIView!
    let streamPlayer: VLCMediaPlayer = {
        let player = VLCMediaPlayer()
        return player
        
    } ()
    let secondStream: VLCMediaPlayer = {
        let player = VLCMediaPlayer()
        return player
        
    } ()
    let thirdStream: VLCMediaPlayer = {
        let player = VLCMediaPlayer()
        return player
        
    } ()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        streamPlayerSetup()
    }
    func streamPlayerSetup() {
        let streamURL = URL(string:"rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov")! //pretpostavlja se da je url valjan - inace if let ...
        let media = VLCMedia(url:streamURL)
        let secondMedia = VLCMedia(url:streamURL)
        let thirdMedia = VLCMedia(url:streamURL)
        streamPlayer.media = media
        streamPlayer.drawable = playerView
        streamPlayer.play()
        streamPlayer.libraryInstance.debugLogging = true
        
        secondStream.media = secondMedia
        secondStream.drawable = secondPlayerView
        secondStream.play()
        secondStream.libraryInstance.debugLogging = true
        
        thirdStream.media = thirdMedia
        thirdStream.drawable = thirdPlayerView
        thirdStream.play()
        thirdStream.libraryInstance.debugLogging = true
        
        
        
        
    }
}
